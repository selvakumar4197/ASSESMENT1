$(document).ready(function(){
	$("#submit").attr('disabled','disabled');
	var ct = 0;
	function sub() {
		var x = $(".valid-feedback");
		for (var i = 0; i < x.length; i++) {
			x[i].style.color =="green" ? ct++ : "";
		}
		ct == 8 ? $("#submit").attr('disabled',false) : $("#submit").attr('disabled','disabled');
		ct = 0;

	}
	function requireed(athis)
	{
		if(athis.val()=="")
		{
			athis.next().css({display:'block' , color:'red'}).html("REQUIRED");
			athis.removeClass('is-valid');
			athis.addClass('is-invalid');
			return 0;
		}
		else
		{
			athis.removeClass('is-invalid');
			athis.addClass('is-valid');
			return 1;
		}
	}
	function nameval(athi)
	{
		athi.val(athi.val().replace(/[0-9]/g,''))
		if(athi.val().length < 3)
		{
			athi.next().css({display:'block' , color:'red'}).html("Name should be more than the 3 letters");
			athi.removeClass('is-valid');
			athi.addClass('is-invalid');
			sub();
		}
		else
		{
			athi.next().css({display:'block' , color:'green'}).html("NAME IS  CORRECT");
			sub();
		}
	}
	function checkEmail(email) 
	{
    	var reg =/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
    	reg.test(email.val())? email.removeClass('is-invalid').addClass('is-valid').next().css({display:'block' , color:'green'}).html("EMAIL IS CORRECT") :email.removeClass('is-valid').addClass('is-invalid').next().css({display:'block' , color:'red'}).html("EMAIL IS NOT  CORRECT"); 
    	sub();
	}

	function password(email)
	{
		var reg =/^(?=.*\d)(?=.*[a-z]).{4,20}$/;
    	reg.test(email.val())? email.removeClass('is-invalid').addClass('is-valid').next().css({display:'block' , color:'green'}).html("PASSWORD MACHED") :email.removeClass('is-valid').addClass('is-invalid').next().css({display:'block' , color:'red'}).html("PASSWORD IS NOT MACHED");
    	sub();
	}

	function mobile(email)
	{
		var reg =/^[6-9][0-9]{9}$/;
    	reg.test(email.val())? email.removeClass('is-invalid').addClass('is-valid').next().css({display:'block' , color:'green'}).html("NUMBER IS  CORRECT") :email.removeClass('is-valid').addClass('is-invalid').next().css({display:'block' , color:'red'}).html("NUMBER IS NOT  CORRECT");
    	sub();
	}

	function userid(email)
	{
		var reg =/^(?=.*[a-zA-Z])[^\*\s]{4,30}$/;
    	reg.test(email.val())? email.removeClass('is-invalid').addClass('is-valid').next().css({display:'block' , color:'green'}).html("NUMBER IS  CORRECT") :email.removeClass('is-valid').addClass('is-invalid').next().css({display:'block' , color:'red'}).html("NUMBER IS NOT  CORRECT");
    	sub();
	}
	$("input, select, textarea").on("input ",function(){

		if(requireed($(this)))
		{
			$(this).attr('id')=="v1"?nameval($(this)):"";
			$(this).attr('id')=="v2"?nameval($(this)):"";
			$(this).attr('id')=="v3"?nameval($(this)):"";
			$(this).attr('id')=="v4"?nameval($(this)):"";
			$(this).attr('id')=="v5"?userid($(this)):"";
			$(this).attr('id')=="e1"?checkEmail($(this)):"";
			$(this).attr('id')=="p1"?password($(this)):"";
			$(this).attr('id')=="m1"?mobile($(this)):"";
		}
	});
});		