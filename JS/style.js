var common,data,a1,a2,a3,a4,a5,a6,a7,a8,b1,b2,q,takei,contask,minutes,hourupdate,x = 1;
// ready function 

$(document).ready(function(){
	var dt = new Date();
	var time1 = dt.toDateString() +" "+ dt.getHours() + " " + dt.getMinutes();
	var sessiontimein = dt.toDateString() +" "+ dt.getHours() + ":" + dt.getMinutes();
	sessionStorage.setItem("sessiontimein", sessiontimein);
	var dth=time1.split(" ");
	if((dth[0] == "Sun") || (dth[0]) == "Sat")
	{
		$("#login").attr('disabled','disabled');
		$("#cpassword").attr('disabled','disabled');
		alert("Sorry today is holiday so You can't work anymore in this tool");
	}
	callready();
});

// call the ready function

function callready()
{
	data = JSON.parse($("#sdata").html());
	getData() == null ? putData(data) : common = JSON.parse(localStorage.getItem("folder"));
	var k=getData();
	var store=ssession();
	chart(common,0);
    function getData()
    {
      return JSON.parse(localStorage.getItem("folder"));
    }
    function putData(data)
    {
      localStorage.setItem("folder" , JSON.stringify(data));
      common = JSON.parse(localStorage.getItem("folder"));
    }
}

// session storage
function ssession()
{
	if(sessionStorage.getItem("userid") != "")
	{
		q = sessionStorage.getItem("userid");
		$(".ha").append(" "+q);
	}
}

// user user id && password check    redirect to: USER PAGE

$(document).on('click','#login',function(){
	var id=$('#userid').val();
	var word=$('#password').val();
	var contask=$('#taskcondition').val();
	var count=0;
	var dt = new Date();
	var time1 = dt.toDateString() +" "+ dt.getHours() + " " + dt.getMinutes();
	var sessiontimein = dt.toDateString() +" "+ dt.getHours() + ":" + dt.getMinutes();
	sessionStorage.setItem("sessiontimein", sessiontimein);
	// cache.put(url, response);
	var dth=time1.split(" ");
	for(var i in common["root"][0]["userdetail"])
	{
		count++;
		if((common["root"][0]["userdetail"][i].id == id) && (common["root"][0]["userdetail"][i].password == word) && ((contask == "taska") || (contask == "taskb")) )
		{
			if(sessionStorage.getItem("userid") != id)
			{
				count--;
				sessionStorage.setItem("userid", id);
				sessionStorage.setItem("password", word);
				sessionStorage.setItem("takei", i);
				sessionStorage.setItem("taskataskb",contask);
				common["root"][0]["userdetail"][i].date[0][contask][0].date.push(sessiontimein);	
				common["root"][0]["userdetail"][i].project[0][contask][0].checkin[0].date.push(dth[2]);
				common["root"][0]["userdetail"][i].project[0][contask][0].checkin[0].day.push(dth[0]);
				common["root"][0]["userdetail"][i].project[0][contask][0].checkin[0].hours.push(dth[4]);
				common["root"][0]["userdetail"][i].project[0][contask][0].checkin[0].minute.push(dth[5]);
				common["root"][0]["userdetail"][i].project[0][contask][0].checkin[0].month.push(dth[1]);
				common["root"][0]["userdetail"][i].project[0][contask][0].checkin[0].year.push(dth[3]);
				localStorage.setItem("folder",JSON.stringify(common));
				window.location.replace("user.html");
			}
			else
			{
				alert("your session is already under open");
			}
		}
	}
	if(count>i)
	{
		alert("user id and password does not match");
	}
});

// admin user id && password check    redirect to: ADMIN PAGE

$(document).on('click','#alogin',function(){
	var id=$('#auserid').val();
	var word=$('#apassword').val();
	for(var i in common["root"][0]["admindetail"])
	{
		if((common["root"][0]["admindetail"][0].id == id) && (common["root"][0]["admindetail"][0].password == word))
		{
			// admin(sessionStorage.setItem("userid", id),sessionStorage.setItem("password", word))
			window.location.replace("admin.html");
			sessionStorage.setItem("userid", id);
			sessionStorage.setItem("password", word);
		}
	}
});

// redirect to admin login page

$(document).on('click','#admin',function(){
	window.location.replace("index1.html");
});

// redirect to initial front page

$(document).on('click','.backi',function(){
	var dt = new Date();
	var time1 = dt.toDateString() +" "+ dt.getHours() + " " + dt.getMinutes();
	dth=time1.split(" ");
	var sessiontimeout = dt.toDateString() +" "+ dt.getHours() + ":" + dt.getMinutes();
	var diff = (new Date(sessiontimeout) - new Date(sessionStorage.getItem("sessiontimein")));
	minutes = diff / (60*1000);
	var hours = minutes / (60*60*1000);
	var days = hours / (24*60*60*1000);
	var time = days + ":" + hours % 24 + ":" + minutes % 60;
	hourupdate = hours.toFixed(20);
	contask = sessionStorage.getItem("taskataskb");
	var findlenght = common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].project[0][contask][0].checkout[0].date.length;
	common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].date[0][contask][0].date.push(sessiontimeout);
	common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].date[0][contask][0].hours.push(parseInt(hourupdate));	
	common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].date[0][contask][0].minute.push(minutes);	

	common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].project[0][contask][0].checkout[0].date.push(dth[2]);
	common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].project[0][contask][0].checkout[0].day.push(dth[0]);
	common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].project[0][contask][0].checkout[0].hours.push(dth[4]);
	common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].project[0][contask][0].checkout[0].minute.push(dth[5]);
	common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].project[0][contask][0].checkout[0].month.push(dth[1]);
	common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].project[0][contask][0].checkout[0].year.push(dth[3]);
	var t = common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].date[0][contask][0].tdate.length;
	if( common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].project[0][contask][0].checkout[0].date[findlenght-1] == dth[2])
	{
		common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].date[0][contask][0].tday[0] = parseInt(common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].date[0][contask][0].tday[0])+minutes;
		common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].date[0][contask][0].thours[0] = parseInt(common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].date[0][contask][0].thours[0])+parseInt(hourupdate);
		localStorage.setItem("folder",JSON.stringify(common));
	}
	else
	{
		common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))]["date"][0][contask][0]["tdate"].push(dth[2]);
		common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))]["date"][0][contask][0]["thours"].push(parseInt(hourupdate));
		common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))]["date"][0][contask][0]["tday"].push(minutes);


		// common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].project[0][contask][0].checkout[0].tdate.push(dth[2]);
		// common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].date[0][contask][0].thours.push(parseInt(hourupdate));	
		// common["root"][0]["userdetail"][parseInt(sessionStorage.getItem("takei"))].date[0][contask][0].tday.push(minutes);
	}
	sessionStorage.clear();
	localStorage.setItem("folder",JSON.stringify(common));
	window.location.replace("index.html");
});

// logout user

$(document).on('click','.logout,.backi1',function(){
	sessionStorage.clear();
	window.location.replace("index.html");
});

// logout admin

$(document).on('click','.logouta',function(){
	sessionStorage.clear();
	window.location.replace("index1.html");
});

// change password

$(document).on('click','#cpassword',function(){
	$('#selva').replaceWith('<div class="col-md-4" id="selva"><form><div class="form-group font-weight-bold"><label for="exampleInputEmail1">User Id</label><input type="text" class="form-control" placeholder="User Id" id="userid"></div><div class="form-group font-weight-bold"><label for="exampleInputPassword1">Old Password</label><input type="password" class="form-control" id="password" placeholder="Old Password"><div class="qwe"><p><small>**PLEASE CONTACT THE SYSTEM ADMIN IF NOT REMEMBER THE PASSWORD</small></p></div></div><div class="form-group font-weight-bold"><label for="exampleInputPassword1">New Password</label><input type="password" class="form-control" id="newpassword" placeholder="New Password"></div><button type="button" class="btn btn-outline-primary font-weight-bold" id="confirm">Confirm<i class="fas fa-arrow-right pl-2"></i></button><button type="button" class="btn btn-outline-secondary float-right font-weight-bold backi1 ml-5"><i class="fas fa-arrow-left pr-2"></i>back</button></form></div>');
});

// check to change the password

$(document).on('click','#confirm',function(){
	var id=$('#userid').val();
	var word=$('#password').val();
	var nword=$('#newpassword').val();
	for(var i in common["root"][0]["userdetail"])
	{
		if((common["root"][0]["userdetail"][i].id == id) && (common["root"][0]["userdetail"][i].password == word))
		{
			common["root"][0]["userdetail"][i].password = nword;
		    localStorage.setItem("folder",JSON.stringify(common));
		    alert("Your password is "+nword+" reset successfully                              Please login with new password");
		    // $('#selva').replaceWith('<div class="col-md-3" id="selva"><form><div class="form-group font-weight-bold"><label for="exampleInputEmail1">User Id</label><input type="text" class="form-control" placeholder="User Id" id="userid"></div><div class="form-group font-weight-bold"><label for="exampleInputPassword1">Password</label><input type="password" class="form-control" id="password" placeholder="Password"></div><button type="button" class="btn btn-outline-primary font-weight-bold" id="login">Log in</button><button type="button" class="btn btn-outline-secondary float-right" id="cpassword">Change Password</button></form></div>');
		    // callready();
		    window.location.replace("index.html");
		}
	}
});

// add employee


$(document).on('click','#addemployee',function(){
	$("#addtable").html("");
	$('#addtable').replaceWith('<div class="row mt-5" id="addtable"><div class="col-md-2"></div><div class="col-md-6 mx-auto"><b>Update button get enable only if you enter the valid data</b></div><div class="col-md-6"><div class="col-md-12 mb-3"><label for="validationServer01">User id</label><input type="text" class="form-control is-valid name" id="v5" placeholder="User id"><div class="valid-feedback" id="v51"></div></div><div class="col-md-12 mb-3"><label for="validationServer02">Password</label><input type="password" class="form-control is-valid"  id="p1" placeholder="Password"><div class="valid-feedback" id="p11"></div></div><div class="col-md-12 mb-3"><label for="validationServer01">First name</label><input type="text" class="form-control is-valid name" id="v1" placeholder="First name"><div class="valid-feedback" id="v11"></div></div><div class="col-md-12 mb-3"><label for="validationServer02">Last name</label><input type="text" class="form-control is-valid name" id="v2" placeholder="Last name"><div class="valid-feedback" id="v21"></div></div><div class="col-md-12 mb-3"><label for="validationServer02">Email</label><input type="text" class="form-control is-valid"  id="e1" placeholder="EMAIL"><div class="valid-feedback" id="e11"></div></div><div class="col-md-12 mb-3"><label for="validationServer02">Phone Number</label><input type="text" class="form-control is-valid"  id="m1" placeholder="Mobile"><div class="valid-feedback" id="m11"></div></div><div class="col-md-12 mb-3"><label for="validationServer01">Father name</label><input type="text" class="form-control is-valid name" id="v3" placeholder="Fathername"><div class="valid-feedback" id="v31"></div></div><div class="col-md-12 mb-3"><label for="validationServer01">Contact Person</label><input type="text" class="form-control is-valid name" id="v4" placeholder="Contact person"><div class="valid-feedback" id="v41"></div></div><button type="button" class="btn btn-outline-secondary float-left font-weight-bold removeback  mt-2"><i class="fas fa-arrow-left pr-2"></i>back</button><button type="button" class="btn btn-outline-secondary float-right font-weight-bold ml-5 mt-2" id="submit">UPDATE<i class="fas fa-upload ml-2"></i></button></div></div>');
	$("#submit").attr('disabled','disabled');
	call();
	x=1;
});


// click update employee

$(document).on('click','#submit',function(){
	a1 = $('#v5').val();
	a2 = $('#p1').val();
	a3 = $('#v1').val();
	a4 = $('#v2').val();
	a5 = $('#v3').val();
	a6 = $('#m1').val();
	a7 = $('#e1').val();
	a8 = $('#v4').val();
	var myObj = {  
               "id":a1,
               "password":a2,
               "firstname":a3,
               "lastname":a4,
               "fathersname":a5,
               "phonenumber":a6,
               "email":a7,
               "date":[  
                  {  
                     "date":[  

                     ],
                     "hours":[  

                     ],
                     "minute":[  

                     ],
                     "taska":[  
                        {  
                           "day":[  

                           ],
                           "date":[  
                              
                           ],
                           "hours":[  
                           
                           ],
                           "tday":[  
           
                           ],
                           "tdate":[  
                             
                           ],
                           "thours":[  
                            
                           ],
                           "minute":[  
                           
                           ]
                        }
                     ],
                     "taskb":[  
                        {  
                           "day":[  

                           ],
                           "date":[  

                           ],
                           "hours":[  

                           ],
                           "tday":[  

                           ],
                           "tdate":[  

                           ],
                           "thours":[  

                           ],
                           "minute":[  

                           ]
                        }
                     ]
                  }
               ],
               "contactperson":"vinoth",
               "project":[  
                  {  
                     "taska":[  
                        {  
                           "checkin":[  
                              {  
                                 "date":[  
                                    
                                 ],
                                 "year":[  
                                   
                                 ],
                                 "day":[  
                                   
                                 ],
                                 "hours":[  
                                    
                                 ],
                                 "month":[  
                                    
                                 ],
                                 "minute":[  
                                   
                                 ]
                              }
                           ],
                           "checkout":[  
                              {  
                                 "date":[  
                                   
                                 ],
                                 "year":[  
                                    
                                 ],
                                 "day":[  
                                    
                                 ],
                                 "hours":[  
                                    
                                 ],
                                 "month":[  
                                   
                                 ],
                                 "tday":[  

                                 ],
                                 "tdate":[  

                                 ],
                                 "thours":[  

                                 ],
                                 "minute":[  
                                    
                                 ]
                              }
                           ]
                        }
                     ],
                     "taskb":[  
                        {  
                           "checkin":[  
                              {  
                                 "date":[  

                                 ],
                                 "year":[  

                                 ],
                                 "day":[  

                                 ],
                                 "hours":[  

                                 ],
                                 "month":[  

                                 ],
                                 "tday":[  

                                 ],
                                 "tdate":[  

                                 ],
                                 "thours":[  

                                 ],
                                 "minute":[  

                                 ]
                              }
                           ],
                           "checkout":[  
                              {  
                                 "date":[  

                                 ],
                                 "year":[  

                                 ],
                                 "day":[  

                                 ],
                                 "hours":[  

                                 ],
                                 "month":[  

                                 ],
                                 "minute":[  

                                 ]
                              }
                           ]
                        }
                     ]
                  }
               ]
             };
	common["root"][0]["userdetail"].push(myObj);
	localStorage.setItem("folder",JSON.stringify(common));
	window.location.replace("admin.html");
	alert("You cannot change once u upload it");
});

// click to remove button on top

$(document).on('click','#removeemployee',function(){
	$("#addtable").html("");
	$('#addtable').replaceWith('<div class="row mt-5" id="addtable"><div class="col-md-2"></div><div class="col-md-6 mx-auto"><b>PLEASE ENTER THE VALID USER IS AND PASSWORD TO REMOVE</b></div><div class="col-md-6"><form><div class="col-md-12 mb-3"><label for="validationServer01">User id</label><input type="text" class="form-control is-valid name" id="dad" placeholder="User id"><div class="valid-feedback"></div></div><div class="col-md-12 mb-3"><label for="validationServer02">Password</label><input type="password" class="form-control is-valid"  id="mom" placeholder="Password"><div class="valid-feedback"></div></div><button type="button" class="btn btn-outline-secondary float-left font-weight-bold removeback  mt-2"><i class="fas fa-arrow-left pr-2"></i>back</button><button type="button" class="btn btn-outline-secondary float-right font-weight-bold ml-5 mt-2 removeemployeebutton">REMOVE & UPDATE<i class="fas fa-upload ml-2"></i></button></form></div></div>');
	x=1;
});

// click remove employee

$(document).on('click','.removeemployeebutton',function(){
	var id=$('#dad').val();
	var word=$('#mom').val();
	var count=0;
	for(var i in common["root"][0]["userdetail"])
	{
		count++;
		if((common["root"][0]["userdetail"][i].id == id) && (common["root"][0]["userdetail"][i].password == word))
		{
			count--;
			 common["root"][0]["userdetail"].splice(i,1);
			localStorage.setItem("folder",JSON.stringify(common));
			alert("successfully removed "+id);
			window.location.replace("admin.html");
		}
	}
	if(count>i)
	{
		alert("user id and password does not match So you cannot remove user");
	}
	window.location.replace("admin.html");
	x=1;
});

// back button for add and remove employee

$(document).on('click','.removeback',function(){
	window.location.replace("admin.html");
});

// table view

$(document).on('click','#tableview',function(){
var table_body = '<table border="1" id="example"><thead><tr><th>S.NO</th><th>ID</th><th>FIRSTNAME</th><th>LASTNAME</th><th>FATHERSNAME</th><th>PHONENUMBER</th><th>EMAIL</th></tr></thead><tbody>';
for(var i in common["root"])
{
	for(var h in common["root"][0]["userdetail"])
	{
        table_body+='<tr>';

        table_body +='<td>';
        table_body +=(parseInt(h)+1);
        table_body +='</td>';

        table_body +='<td>';
        table_body +=common["root"][0]["userdetail"][h].id;
        table_body +='</td>';

       
        table_body +='<td>';
        table_body +=common["root"][0]["userdetail"][h].firstname;
        table_body +='</td>';

        table_body +='<td>';
        table_body +=common["root"][0]["userdetail"][h].lastname;
        table_body +='</td>';

        table_body +='<td>';
        table_body +=common["root"][0]["userdetail"][h].fathersname;
        table_body +='</td>';

        table_body +='<td>';
        table_body +=common["root"][0]["userdetail"][h].phonenumber;
        table_body +='</td>';

        table_body +='<td>';
        table_body +=common["root"][0]["userdetail"][h].email;
        table_body +='</td>';
    
        table_body+='</tr>';
    }
}
    table_body+='</tbody></table>';
   $('#addtable').html(table_body);
});

// search is box

$(document).on("keyup","#isbox", function() {
var value = $(this).val().toLowerCase();
	$("table tr").filter(function(index){
	if(index>0)
		{
		$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		}
	});
});

// filter

$(document).on('change','#cate1',function(){
       x=$('#cate,option:selected').val();
       var selva=1;
       if(x == "0")
       {
       	var x=[];
       	var x1=[];
        var table_body = '<table border="1" id="example"><thead><tr><th>S.NO</th><th>ID</th><th>MINUTE</th><th>HOUR</th></tr></thead><tbody>';
		for(var i in common["root"][0]["userdetail"])
		{    
			// for(var h in common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taska"][0].date)
			// {
				table_body+='<tr>';

		        table_body +='<td>';
		        table_body +=selva++;
		        table_body +='</td>';

		        table_body +='<td>';
		        table_body +=common["root"][0]["userdetail"][parseInt(i)].id;
		        table_body +='</td>';
		        

		        // if(h % 2 == 0)
		        // {
		        // 	table_body +='<td>';
			       //  table_body +=" Checkin time =  "+common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taska"][0].date[parseInt(h)];
			       //  table_body +='</td>';
		        // }
		        // else
		        // {
		        // 	table_body +='<td>';
			       //  table_body +=" Checkout time =  "+common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taska"][0].date[parseInt(h)];
			       //  table_body +='</td>';
		        // }

		        x[parseInt(i)] = 0;
		    	for(var j in common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taska"][0]["tday"])
		    	{
		    		x[parseInt(i)] += common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taska"][0]["tday"][parseInt(j)];
		    	}

		        table_body +='<td>';
		        table_body +=x[parseInt(i)]+" minutes";
		        table_body +='</td>';

		        x1[parseInt(i)] = 0;
		    	for(var j in common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taska"][0]["tday"])
		    	{
		    		x1[parseInt(i)] += common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taska"][0]["thours"][parseInt(j)];
		    	}

		        table_body +='<td>';
		        table_body +=+x1[parseInt(i)]+" hours";
		        table_body +='</td>';

		        table_body+='</tr>';
		        // }
		}
	    table_body+='</tbody></table>';
	   $('#addtable').html(table_body);
       }
       if(x == "1")
       {
        var table_body = '<table border="1" id="example"><thead><tr><th>S.NO</th><th>ID</th><th>MINUTE</th><th>HOUR</th></tr></thead><tbody>';
        var y = [];
        var y1= [];
		for(var i in common["root"][0]["userdetail"])
		{	        
			// for(var h in common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taskb"][0].date)
			// {
				table_body+='<tr>';

		        table_body +='<td>';
		        table_body +=selva++;
		        table_body +='</td>';

		        table_body +='<td>';
		        table_body +=common["root"][0]["userdetail"][parseInt(i)].id;
		        table_body +='</td>';
		        
		        // if(h % 2 == 0)
		        // {
		        // 	table_body +='<td>';
			       //  table_body +=" Checkin time =  "+common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taskb"][0].date[parseInt(h)];
			       //  table_body +='</td>';
		        // }
		        // else
		        // {
		        // 	table_body +='<td>';
			       //  table_body +=" Checkout time =  "+common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taskb"][0].date[parseInt(h)];
			       //  table_body +='</td>';
		        // }
		        

		        y[parseInt(i)] = 0;
		    	for(var j in common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taskb"][0]["tday"])
		    	{
		    		y[parseInt(i)]+=common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taskb"][0]["tday"][parseInt(j)];
		    	}

		        table_body +='<td>';
		        table_body +=y[parseInt(i)]+" minutes";
		        table_body +='</td>';

		        y1[parseInt(i)] = 0;
		    	for(var j in common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taskb"][0]["tday"])
		    	{
		    		y1[parseInt(i)] += common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taskb"][0]["thours"][parseInt(j)];
		    	}

		        table_body +='<td>';
		        table_body +=+y1[parseInt(i)]+" hours";
		        table_body +='</td>';

		        table_body+='</tr>';
		        // }
		}
    table_body+='</tbody></table>';
   $('#addtable').html(table_body);  

       }
       if(x == "2")
       {
       	var table_body = '<table border="1" id="example"><thead><tr><th>S.NO</th><th>ID</th><th>PASSWORD</th><th>FIRSTNAME</th><th>LASTNAME</th><th>FATHERSNAME</th><th>PHONENUMBER</th><th>EMAIL</th></tr></thead><tbody>';
		for(var i in common["root"])
		{
			for(var h in common["root"][0]["userdetail"])
			{
		        table_body+='<tr>';

		        table_body +='<td>';
		        table_body +=(parseInt(h)+1);
		        table_body +='</td>';

		        table_body +='<td>';
		        table_body +=2;
		        table_body +='</td>';

		        table_body +='<td>';
		        table_body +=common["root"][0]["userdetail"][h].password;
		        table_body +='</td>';
		       
		        table_body +='<td>';
		        table_body +=common["root"][0]["userdetail"][h].firstname;
		        table_body +='</td>';

		        table_body +='<td>';
		        table_body +=common["root"][0]["userdetail"][h].lastname;
		        table_body +='</td>';

		        table_body +='<td>';
		        table_body +=common["root"][0]["userdetail"][h].fathersname;
		        table_body +='</td>';

		        table_body +='<td>';
		        table_body +=common["root"][0]["userdetail"][h].phonenumber;
		        table_body +='</td>';

		        table_body +='<td>';
		        table_body +=common["root"][0]["userdetail"][h].email;
		        table_body +='</td>';
		    
		        table_body+='</tr>';
		        }
		}
    table_body+='</tbody></table>';
   $('#addtable').html(table_body);
       }
       
    });

// chart
var x=[];
var y=[];
function chart(common){
	try {
    //Sales chart
    // if(refresh == 1)
    // {
    	for(var i in common["root"][0]["userdetail"])
	    {
	    	x[parseInt(i)] = 0;
	    	for(var j in common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taska"][0]["tday"])
	    	{
	    		x[parseInt(i)]+=common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taska"][0]["tday"][parseInt(j)];
	    	}
	    }
	    for(var i in common["root"][0]["userdetail"])
	    {
	    	y[parseInt(i)] = 0;
	    	for(var j in common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taskb"][0]["tday"])
	    	{
	    		y[parseInt(i)]+=common["root"][0]["userdetail"][parseInt(i)]["date"][0]["taskb"][0]["tday"][parseInt(j)];
	    	}
	    }
    // }
    var ctx = document.getElementById("sales-chart");
    if (ctx) {
      ctx.height = 150;
      var labless=[];
      for( var i in common["root"][0]["userdetail"])
      {
      	labless.push(common["root"][0]["userdetail"][i].id);
      }
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: labless,
          type: 'line',
          defaultFontFamily: 'Poppins',
          datasets: [{
            label: "TASK A",
            data: x,
            backgroundColor: 'transparent',
            borderColor: 'rgba(220,53,69,0.75)',
            borderWidth: 3,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: 'rgba(220,53,69,0.75)',
          }, {
            label: "TASK B",
            data: y,
            backgroundColor: 'transparent',
            borderColor: 'rgba(40,167,69,0.75)',
            borderWidth: 3,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: 'rgba(40,167,69,0.75)',
          }]
        },
        options: {
          responsive: true,
          tooltips: {
            mode: 'index',
            titleFontSize: 12,
            titleFontColor: '#000',
            bodyFontColor: '#000',
            backgroundColor: '#fff',
            titleFontFamily: 'Poppins',
            bodyFontFamily: 'Poppins',
            cornerRadius: 3,
            intersect: false,
          },
          legend: {
            display: false,
            labels: {
              usePointStyle: true,
              fontFamily: 'Poppins',
            },
          },
          scales: {
            xAxes: [{
              display: true,
              gridLines: {
                display: false,
                drawBorder: false
              },
              scaleLabel: {
                display: true,
                labelString: 'EMPLOYEE NAME'
              },
              ticks: {
                fontFamily: "Poppins"
              }
            }],
            yAxes: [{
              display: true,
              gridLines: {
                display: false,
                drawBorder: false
              },
              scaleLabel: {
                display: true,
                labelString: 'TOTAL WORK IN TASKA AND TASKB',
                fontFamily: "Poppins"

              },
              ticks: {
                fontFamily: "Poppins",
                // beginAtZero: true,
                // stepSize: 1
              }
            }]
          },
          title: {
            display: false,
            text: 'Normal Legend'
          }
        }
      });
    }


  } catch (error) {
    console.log(error);
  }
}
	var ct = 0;
	function sub() {
		var x = $(".valid-feedback");
		for (var i = 0; i < x.length; i++) {
			x[i].style.color =="green" ? ct++ : "";
		}
		ct == 8 ? $("#submit").attr('disabled',false) : $("#submit").attr('disabled','disabled');
		ct = 0;

	}
	function requireed(athis)
	{
		if(athis.val()=="")
		{
			athis.next().css({display:'block' , color:'red'}).html("REQUIRED");
			athis.removeClass('is-valid');
			athis.addClass('is-invalid');
			return 0;
		}
		else
		{
			athis.removeClass('is-invalid');
			athis.addClass('is-valid');
			return 1;
		}
	}
	function nameval(athi)
	{
		athi.val(athi.val().replace(/[0-9]/g,''))
		if(athi.val().length < 3)
		{
			athi.next().css({display:'block' , color:'red'}).html("Name should be more than the 3 letters");
			athi.removeClass('is-valid');
			athi.addClass('is-invalid');
			sub();
		}
		else
		{
			athi.next().css({display:'block' , color:'green'}).html("NAME IS  CORRECT");
			sub();
		}
	}
	function checkEmail(email) 
	{
    	var reg =/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
    	reg.test(email.val())? email.removeClass('is-invalid').addClass('is-valid').next().css({display:'block' , color:'green'}).html("EMAIL IS CORRECT") :email.removeClass('is-valid').addClass('is-invalid').next().css({display:'block' , color:'red'}).html("EMAIL IS NOT  CORRECT"); 
    	sub();
	}

	function password(email)
	{
		var reg =/^(?=.*\d)(?=.*[a-z]).{4,20}$/;
    	reg.test(email.val())? email.removeClass('is-invalid').addClass('is-valid').next().css({display:'block' , color:'green'}).html("PASSWORD MACHED") :email.removeClass('is-valid').addClass('is-invalid').next().css({display:'block' , color:'red'}).html("PASSWORD IS NOT MACHED");
    	sub();
	}

	function mobile(email)
	{
		var reg =/^[6-9][0-9]{9}$/;
    	reg.test(email.val())? email.removeClass('is-invalid').addClass('is-valid').next().css({display:'block' , color:'green'}).html("NUMBER IS  CORRECT") :email.removeClass('is-valid').addClass('is-invalid').next().css({display:'block' , color:'red'}).html("NUMBER IS NOT  CORRECT");
    	sub();
	}

	function userid(email)
	{
		var reg =/^(?=.*[a-zA-Z])[^\*\s]{4,30}$/;
    	reg.test(email.val())? email.removeClass('is-invalid').addClass('is-valid').next().css({display:'block' , color:'green'}).html("NUMBER IS  CORRECT") :email.removeClass('is-valid').addClass('is-invalid').next().css({display:'block' , color:'red'}).html("NUMBER IS NOT  CORRECT");
    	sub();
	}
	function call()
	{
		$(document).on("input","#v1,#v2,#v3,#v4,#v5,#m1,#p1,#e1",function(){

		if(requireed($(this)))
		{
			$(this).attr('id')=="v1"?nameval($(this)):"";
			$(this).attr('id')=="v2"?nameval($(this)):"";
			$(this).attr('id')=="v3"?nameval($(this)):"";
			$(this).attr('id')=="v4"?nameval($(this)):"";
			$(this).attr('id')=="v5"?userid($(this)):"";
			$(this).attr('id')=="e1"?checkEmail($(this)):"";
			$(this).attr('id')=="p1"?password($(this)):"";
			$(this).attr('id')=="m1"?mobile($(this)):"";
		}
		});
	}